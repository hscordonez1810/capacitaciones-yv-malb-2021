![steveen ordoñez](https://gitlab.com/hscordonez1810/capacitaciones-yv-malb-2021/-/raw/master/trabajo/biografia.md/imagenes/steven.jpg) 


# enalace

[gitlab](https://gitlab.com/hscordonez1810/capacitaciones-yv-malb-2021/-/blob/master/trabajo/gitlab/gitlab.md)

[git](https://gitlab.com/hscordonez1810/capacitaciones-yv-malb-2021/-/blob/master/trabajo/git.md/git.md)

<br>

# Menu

>- [git](#https://gitlab.com/hscordonez1810/capacitaciones-yv-malb-2021/-/blob/master/git.md/git.md)
>- [Datos Personales](#DatosPersonales)
>- [Aspiraciones_Estudios](#Aspiraciones_Estudios)
>- [Pasatiempos](#Pasatiempos)


# Datos Personales


>Mi nombre es steveen Ordoñez tengo 20 años y 5 hermanos vivo con mis padres nací en Quito, mi fecha de cumpleaños es el 18 de marzo, mi signo zodiacal es piscis con ascendencia a acuario, mi estatura es de 1.70, la música que me gusta es rock alternativo, indi, rock, punk, y mis bandas favoritas son the strokes soda stereo, ska-p, the Smith, Zoé.

# Aspiraciones_Estudios

[ir al inicio](#menu)

>Culmine con mis estudios de bachillerato en el 2020 en el colegio Replica ‘’Juan Pio’’ Montufar lo que fue de manera virtual por la pandemia del coronavirus. Mis aspiraciones son poder grabar un álbum de música de rock alternativo además de esto quiero lograr graduarme  de programación una vez que logre culminar con esta carrera quisiera seguir una carrera de física aplicada o experimental para poder entender el universo y como se constituye , una de mis aspiraciones  que quiero llegar a cumplir es fundar una empresa de tecnología y desarrollo para poder avanzar con la tecnología y una vez que la empresa logre estar estable poder emprender en formas energía renovable para poder ayudar al planeta.

# Pasatiempos

[ir al inicio](#menu)

>Mis pasatiempos más recurrentes son leer e investigar formas de energía   renovable y llevarlo a la carrera que estoy siguiendo en este momento además toco instrumentos musicales como la guitarra el bajo, ukulele, armónica, piano y algunos instrumentos de percusión lo que me ayuda a crear música en mi banda de rock alternativo, también disfruto de leer sobre todo lo que es drama , misterio  y terror  , asimismo caminar en lugares vacíos con alguien es una forma agradable de pasar tiempo ,también escribir como poesía y libros y novelas aunque no he acabado ningún libro hasta ahora  igualmente dibujar abstractamente y a lápiz. 

