# Enlace

[biografia](https://gitlab.com/hscordonez1810/capacitaciones-yv-malb-2021/-/blob/master/biografia.md/biografia.md)

[git](https://gitlab.com/hscordonez1810/capacitaciones-yv-malb-2021/-/blob/master/git.md/git.md)

# Menu

>- [acceder](#acceder)
>- [repositorios](#repositorios)
>- [grupos](#grupos)
>- [subgrupos ](#subgrupos) 
>- [issues](#issues) 
>- [label](#label) 
>- [permisos](#permisos) 
>- [miembros](#miembros) 
>- [commits](#commits) 



## acceder

>### gitlab como se accede
>se puede acceder desde la pagina oficial https:gitlab.com luego se puede registrar con gmil o si ya tienes una cuenta github puedes logearte a esa cuenta.


## repositorios


>### crear repositorios
>para crear un repositorio vamos a proyectos y buscamos donde dice nuevo proyecto y agregamos el mombre al proyecto colocamos en modo privado o publico y lo iniciamos en reamder

![proyecto](https://gitlab.com/hscordonez1810/capacitaciones-yv-malb-2021/-/raw/master/trabajo/gitlab/imagenes/proyecto.png)

![blanco](https://gitlab.com/hscordonez1810/capacitaciones-yv-malb-2021/-/raw/master/trabajo/gitlab/imagenes/blanco.png)

![repositorio](https://gitlab.com/hscordonez1810/capacitaciones-yv-malb-2021/-/raw/master/trabajo/gitlab/imagenes/repositorio.png)


## grupos

>### crear grupos
>vamos a la parte de menu y buscamos la parte que dice grupos y colocamos crear grupo , colocamos el nombre del proyecto y si deseamos que sea privado o publico.

![grupo1](https://gitlab.com/hscordonez1810/capacitaciones-yv-malb-2021/-/raw/master/trabajo/gitlab/imagenes/grupo1.png)

![grupo](https://gitlab.com/hscordonez1810/capacitaciones-yv-malb-2021/-/raw/master/trabajo/gitlab/imagenes/grupo.png)


## subgrupos

>### crear subgrupos
>en el grupo que tenemos colocamos subgrupo y colocamos lo que deceamos nombre del proyecto y las demas condiciones.

![subgrupo](https://gitlab.com/hscordonez1810/capacitaciones-yv-malb-2021/-/raw/master/trabajo/gitlab/imagenes/subgrupo1.png)

![subgrupo](https://gitlab.com/hscordonez1810/capacitaciones-yv-malb-2021/-/raw/master/trabajo/gitlab/imagenes/subgrupo.png)

## issues

>### crear issues
>en nuestro proyecto buscamos la parte en que dice issues y colocamos lo que deceamos como etiquetas adentro y poder tener referencia de lo que hacemos.

![issues](https://gitlab.com/hscordonez1810/capacitaciones-yv-malb-2021/-/raw/master/trabajo/gitlab/imagenes/issues.png)

## label

>### crear labels
>buscamos la parte de labels o etiquetas y creamos colocamos el nombre y el color que deseamos

![label](https://gitlab.com/hscordonez1810/capacitaciones-yv-malb-2021/-/raw/master/trabajo/gitlab/imagenes/label.png)

## permisos

>### hablar un poco de los permisos y para que sirven 
>para otorgar permisos vamos en la parte de agregar miembro y colocamos los permisos que le vamos a otrogar a esa persona.

![permisos](https://gitlab.com/hscordonez1810/capacitaciones-yv-malb-2021/-/raw/master/trabajo/gitlab/imagenes/permisos.png)

## miembros

>### agregar miembro
>para agregar un miembro vamos a la parte de miembros y le enviamos una envitacion para que puede aceptarla 

![miembros](https://gitlab.com/hscordonez1810/capacitaciones-yv-malb-2021/-/raw/master/trabajo/gitlab/imagenes/miembros.png)
## commits

>### commits desde gitlab y crear ramas desde gitlab
>para crear commits o ramas vamos a la parte principal de nuestro proyecto y buscamos donde dice commits o brach en español compromete y sucursales y donde se encuentra el mas podemos colocar una sucursal o rama.

![ramas](https://gitlab.com/hscordonez1810/capacitaciones-yv-malb-2021/-/raw/master/trabajo/gitlab/imagenes/ramas.png)

