# Enlace

[gitlab](https://gitlab.com/hscordonez1810/capacitaciones-yv-malb-2021/-/blob/master/trabajo/gitlab/gitlab.md)

[biografia](https://gitlab.com/hscordonez1810/capacitaciones-yv-malb-2021/-/blob/master/trabajo/biografia.md/biografia.md)

<br>

# menu

>- [status:](#status:)
>- [add.](#add.)
>- [touch:](#touch:)
>- [gitignore: ](#gitignore:) 
>- [ls:](#ls:) 
>- [init: ](#init:) 
>- [cliente ](#cliente) 
>- [clone ](#clone) 
>- [ramas ](#ramas) 



## Que es git 

>Git nos ayuda de una forma practica a crear proyectos donde varias personas pueden interactuar y desarrollar el proyecto de forma más rápida y practica además que podemos subir repositorios de forma más practica con esto tenemos el control de nuestros proyectos y podemos ver como hemos avanzado y las ramas que hemos creado 

## Comandos en git en consola

>Los comandos mas recurrentes en consola son:

>### status: 
>nos ayuda a ver el estado de nuestro proyecto si esta guardado o no cuando no esta guardado sale en rojo y cuando se guarda es verde.


## No Guardado
![no guardado](https://gitlab.com/hscordonez1810/capacitaciones-yv-malb-2021/-/raw/master/trabajo/git.md/imagenes/status.png)



## Guardado
![guardado](https://gitlab.com/hscordonez1810/capacitaciones-yv-malb-2021/-/raw/master/trabajo/git.md/imagenes/verde.png)

>### add. : 
>sirve para guardar lo que hemos creado.

![add](https://gitlab.com/hscordonez1810/capacitaciones-yv-malb-2021/-/raw/master/trabajo/git.md/imagenes/add.png)



>### git commit -m “ ” : 
>lo guardamos y le colocamos un comentario entre los paréntesis se guardará lo que hemos avanzado.

![commit](https://gitlab.com/hscordonez1810/capacitaciones-yv-malb-2021/-/raw/master/trabajo/git.md/imagenes/commit.png)

>### touch: 
>nos ayuda a crear textos le colocamos touch el nombre y se crea.

![touch](https://gitlab.com/hscordonez1810/capacitaciones-yv-malb-2021/-/raw/master/trabajo/git.md/imagenes/touch.png)

>### gitignore : 
>nos ayuda para crear un archivo donde podemos ocultar otros.

![gitignore](https://gitlab.com/hscordonez1810/capacitaciones-yv-malb-2021/-/raw/master/trabajo/git.md/imagenes/gitignore.png)

>### ls: 
>vemos lo que tenemos en el escritorio y lo que podemos elegir.

![ls](https://gitlab.com/hscordonez1810/capacitaciones-yv-malb-2021/-/raw/master/trabajo/git.md/imagenes/ls.png)

>### init : 
>creamos una carpeta para comenzar con nuestro proyecto.

![init](https://gitlab.com/hscordonez1810/capacitaciones-yv-malb-2021/-/raw/master/trabajo/git.md/imagenes/init.png)

[ir al inicio](#Menu)


## cliente
![init](https://gitlab.com/hscordonez1810/capacitaciones-yv-malb-2021/-/raw/master/trabajo/git.md/imagenes/rama.png)

## clone
>### clonacion de proyecto por consolay por cliente
>buscamos en gitlab el clone y buscamos la forma que nos sirve para poder agregar el enlace en la consola en este caso con el http

![init](https://gitlab.com/hscordonez1810/capacitaciones-yv-malb-2021/-/raw/master/trabajo/git.md/imagenes/http.png)

![init](https://gitlab.com/hscordonez1810/capacitaciones-yv-malb-2021/-/raw/master1/trabajo/git.md/imagenes/consola.png)

## Commit
>### commit por consola y por kraken o smart
>podemos crear commit mediante comandos git commit -m "" y agragamos un commit en kraken es mas sensillo ya que solo agregamos el comentario y le colocamos aceptar y ya se crea el commit 

![init](https://gitlab.com/hscordonez1810/capacitaciones-yv-malb-2021/-/raw/master/trabajo/git.md/imagenes/commit.png)

![init](https://gitlab.com/hscordonez1810/capacitaciones-yv-malb-2021/-/raw/master/trabajo/git.md/imagenes/kraken.png)

![init](https://gitlab.com/hscordonez1810/capacitaciones-yv-malb-2021/-/raw/master/trabajo/git.md/imagenes/krakencommit.png)

## ramas

>### ramas desde kraken
>creamos directamente con branch y podemos agregarle de la forma en que requerimos  le colocamos el nombre en este caso como rama1

![init](https://gitlab.com/hscordonez1810/capacitaciones-yv-malb-2021/-/raw/master/trabajo/git.md/imagenes/branch.png)

![init](https://gitlab.com/hscordonez1810/capacitaciones-yv-malb-2021/-/raw/master/trabajo/git.md/imagenes/rama.png)

[ir al inicio](#Menu)
